import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-forget-pass',
  templateUrl: './forget-pass.page.html',
  styleUrls: ['./forget-pass.page.scss'],
})
export class ForgetPassPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async forgetemail(){
    this.router.navigate(['/forget-pass-email'])
  }

  async forgetphone(){
    this.router.navigate(['/forget-pass-phone'])
  }

}
