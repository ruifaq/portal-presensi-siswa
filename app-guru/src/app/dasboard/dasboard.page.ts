import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { MenuController, ModalController } from '@ionic/angular';
import { ProfilePage } from '../profile/profile.page';


@Component({
  selector: 'app-dasboard',
  templateUrl: './dasboard.page.html',
  styleUrls: ['./dasboard.page.scss'],
})
export class DasboardPage implements OnInit {

  data: any = {};
  userData: any = {};
  jadwalSiswa:any = [];
  waktu = new Date()
  today: any = new Date();

  constructor(
    public router: Router,
    private menu: MenuController,
    public modalController: ModalController,
    private db:AngularFirestore,
    public auth: AngularFireAuth,
  ) { }

  ngOnInit() {

    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })

    this.getJadwal();
  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('user').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }

  async jadwal(){
    this.router.navigate(['/jadwal'])
  }

  async profileInfo(){
    this.router.navigate(['/profile'])
  }

  async rekap(){
    this.router.navigate(['/rekap'])
  }

  async report(){
    this.router.navigate(['/report'])
  }

  async presensi(mapel=String, jam=String, guru=String){
    console.log(mapel, jam, guru)
    this.router.navigate(['/list-presensi-siswa'])
  }

  async presensiSubmit(){
    this.router.navigate(['/presensi-submit'])
  }

  async presensiOffline(){
    this.router.navigate(['/presensi-offline'])
  }

  async profile() {
    const modal = await this.modalController.create({
      component: ProfilePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  getJadwal(){
    this.jadwalSiswa = [];
    this.db.collection('jadwal').valueChanges({idField: 'id'}).subscribe(res => {
      console.log(res);
      this.parsingJadwal(res);
      
    })
  }

  parsingJadwal(data){
    let today = new Date();
    for(let i=0; i < data.length; i++)
    {
      let jam = data[i].jamMasuk;
      let jamArray = jam.split(".");
      console.log(Number(jamArray[0]) + ''  + today.getDay());
      
      if(data[i].hari == today.getDay() && today.getHours() >  Number(jamArray[0]) )
      {
        data[i].isActive = true;
        console.log(Number(jamArray[0]) );
      } else{
        data[i].isActive = false;
      }
    }
    this.jadwalSiswa = data;
  }

  bukaPresensi(jam){
    const A = this.data;
    if( jam < '50'){
      this.router.navigate(['/presensi-form'])
    }else{
      this.router.navigate(['/presensi-late'])
    }
  }

}
