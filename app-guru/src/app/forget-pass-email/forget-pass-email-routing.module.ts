import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForgetPassEmailPage } from './forget-pass-email.page';

const routes: Routes = [
  {
    path: '',
    component: ForgetPassEmailPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForgetPassEmailPageRoutingModule {}
