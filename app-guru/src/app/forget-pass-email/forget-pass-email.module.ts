import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgetPassEmailPageRoutingModule } from './forget-pass-email-routing.module';

import { ForgetPassEmailPage } from './forget-pass-email.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForgetPassEmailPageRoutingModule
  ],
  declarations: [ForgetPassEmailPage]
})
export class ForgetPassEmailPageModule {}
