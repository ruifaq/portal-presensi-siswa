import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router'

@Component({
  selector: 'app-rekap',
  templateUrl: './rekap.page.html',
  styleUrls: ['./rekap.page.scss'],
})
export class RekapPage implements OnInit {

  data: any = {};
  userData: any = {};
  public mapelList: any[];
  public loadedMapelList: any[];

  constructor(
    public router: Router,
    private db:AngularFirestore,
    public auth: AngularFireAuth,
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })

    this.db.collection("jadwal").valueChanges().subscribe(mapelList => {
      this.mapelList = mapelList;
      this.loadedMapelList = mapelList;
    })
  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('user').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }
  
  initializeItems(): void{
    this.mapelList = this.loadedMapelList;
  }

  filterList(evt){
    this.initializeItems();

    const searchTerm = evt.srcElement.value;

    if(!searchTerm) {
      return;
    }

    this.mapelList = this.mapelList.filter(currentMapel => {
      if(currentMapel.kelas && searchTerm) {
        if(currentMapel.kelas.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  async print(){
    this.router.navigate(['/rekap-print'])
  }
  
}
