import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-presensi-form',
  templateUrl: './presensi-form.page.html',
  styleUrls: ['./presensi-form.page.scss'],
})
export class PresensiFormPage implements OnInit {

  data: any = {};
  userData: any = {};
  Siswa: any = [];
dateTanggal = new Date();

  constructor(
    public router:Router,
    private db:AngularFirestore,
    private auth:AngularFireAuth
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(user=>{
      this.userData = user;
    })

    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })

    this.getSiswa();
  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('user').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  async kirim(){
    this.router.navigate(['/presensi-submit'])
  }

  simpanPresensi(){
    this.data.author = this.userData.email;
    var doc = new Date().getTime().toString();
    this.db.collection('presensi-guru').doc(doc).set(this.data).then(res=>{
      this.router.navigate(['/presensi-submit'])
      console.log('presensi berhasil...!', this.data)
    })
  }

getSiswa(){
  this.Siswa = [];
  this.db.collection('siswa').valueChanges({idField: 'id'}).subscribe(res => {
    this.Siswa = res;
  })
}

}
