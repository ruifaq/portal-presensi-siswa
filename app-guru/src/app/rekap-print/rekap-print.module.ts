import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RekapPrintPageRoutingModule } from './rekap-print-routing.module';

import { RekapPrintPage } from './rekap-print.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RekapPrintPageRoutingModule
  ],
  declarations: [RekapPrintPage]
})
export class RekapPrintPageModule {}
