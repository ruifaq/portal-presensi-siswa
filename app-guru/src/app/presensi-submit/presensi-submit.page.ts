import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-presensi-submit',
  templateUrl: './presensi-submit.page.html',
  styleUrls: ['./presensi-submit.page.scss'],
})
export class PresensiSubmitPage implements OnInit {

  data: any = {};
  userData: any = {};
  jadwalSiswa: any = [];

  constructor(
    public router: Router,
    private db:AngularFirestore,
    private auth:AngularFireAuth,
    public alertController: AlertController,
  ) { }

  ngOnInit() {
    this.getPresensi();

    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })

  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('user').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }

  async back(){
    this.router.navigate(['/presensi-form'])
  }

  async next(){
    this.router.navigate(['/dasboard'])
  }

  getPresensi(){
    this.jadwalSiswa = [];
    this.db.collection('presensi-guru').valueChanges({idField: 'id'}).subscribe(res => {
      this.jadwalSiswa = res;
    })
  }

  
  async kirimPresensi(){
    this.router.navigate(['/dasboard'])
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Berhasil Mengisi Presensi',
      message: 'Jika ada kendala dapat menghubungi Staff TU',
      buttons: [
        {
          text: 'Tutup',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        },
      ]
    });

    await alert.present();
  }

}
