import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListPresensiSiswaPage } from './list-presensi-siswa.page';

describe('ListPresensiSiswaPage', () => {
  let component: ListPresensiSiswaPage;
  let fixture: ComponentFixture<ListPresensiSiswaPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListPresensiSiswaPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListPresensiSiswaPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
