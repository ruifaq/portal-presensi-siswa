import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-list-presensi-siswa',
  templateUrl: './list-presensi-siswa.page.html',
  styleUrls: ['./list-presensi-siswa.page.scss'],
})
export class ListPresensiSiswaPage implements OnInit {

  data: any = {};
  userData: any = {};
  Siswa: any = [];
dateTanggal = new Date();
jadwalSiswa: any = [];

  constructor(
    public router:Router,
    private db:AngularFirestore,
    private auth:AngularFireAuth
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(user=>{
      this.userData = user;
    })

    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })

    this.getSiswa();
    this.getPresensi();
  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('user').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  async kirim(){
    this.router.navigate(['/presensi-submit'])
  }

  async next(){
    this.router.navigate(['/presensi-form'])
  }

  simpanPresensi(){
    this.data.author = this.userData.email;
    var doc = new Date().getTime().toString();
    this.db.collection('events').doc(doc).set(this.data).then(res=>{
      this.router.navigate(['/presensi-submit'])
      console.log('presensi berhasil...!', this.data)
    })
  }

getSiswa(){
  this.Siswa = [];
  this.db.collection('siswa').valueChanges({idField: 'id'}).subscribe(res => {
    this.Siswa = res;
  })
}

getPresensi(){
  this.jadwalSiswa = [];
  this.db.collection('events').valueChanges({idField: 'id'}).subscribe(res => {
    this.jadwalSiswa = res;
  })
}

}
