import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ListPresensiSiswaPage } from './list-presensi-siswa.page';

const routes: Routes = [
  {
    path: '',
    component: ListPresensiSiswaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ListPresensiSiswaPageRoutingModule {}
