import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ListPresensiSiswaPageRoutingModule } from './list-presensi-siswa-routing.module';

import { ListPresensiSiswaPage } from './list-presensi-siswa.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ListPresensiSiswaPageRoutingModule
  ],
  declarations: [ListPresensiSiswaPage]
})
export class ListPresensiSiswaPageModule {}
