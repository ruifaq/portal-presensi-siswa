import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ForgetPassPhonePageRoutingModule } from './forget-pass-phone-routing.module';

import { ForgetPassPhonePage } from './forget-pass-phone.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ForgetPassPhonePageRoutingModule
  ],
  declarations: [ForgetPassPhonePage]
})
export class ForgetPassPhonePageModule {}
