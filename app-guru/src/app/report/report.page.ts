import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { report } from 'process';
import { AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { LoadingController, NavController } from '@ionic/angular';
import { AlertController, ModalController } from '@ionic/angular';
import { ImageUploderPage } from '../image-uploder/image-uploder.page';
import { ImageCroppedEvent } from 'ngx-image-cropper';
import { Plugins, CameraResultType } from '@capacitor/core';
import { AlertService } from '../services/alert.service';
import { ToastService } from '../services/toast.service';
import { CropGambarPage } from '../crop-gambar/crop-gambar.page';
const { Camera } = Plugins;

@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

  @Input() imagePath: any;

  data: any = {};
  userData: any = {};
  items: Observable<any[]>;
  newTodo: string = '';
  itemsRef: AngularFirestoreCollection;
  selectedFile: any;
  
  ID: any;
  docID:any;

  constructor(
    public router: Router,
    private db:AngularFirestore,
    private auth:AngularFireAuth,
    private loadingController: LoadingController,
    private storage: AngularFireStorage,
    public modalCtrl: ModalController,
    public alert:AlertService,
    public alertController: AlertController,
    public toast: ToastService,
    public modalController: ModalController,
    private navCtrl: NavController,
    public routes: ActivatedRoute,
    
  ) { }


  ngOnInit() {
    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })
  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('user').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }

  updatingProfileImage: boolean;
  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.DataUrl
    });
    var imageUrl = image.dataUrl;
    var imagePath = this.userData.email+'/laporan.png';
    const modal = await this.modalController.create({
      component: ImageUploderPage,
      cssClass: 'my-custom-class',
      componentProps:{imageData:imageUrl,imagePath: imagePath,ratio:1,width:800,height:800}
    }); 
    modal.onDidDismiss().then(res=>{
      if(res.data.imageUrl != false)
      {
        this.updateUserData({reportUrl:res.data.imageUrl});
        this.updateProfileUrl(res.data.imageUrl);
      }
    });
    return await modal.present(); 
  }

  updateUserData(data)
  {
    this.createReport();
    this.data.author = this.userData.email;
    this.updatingProfileImage = true;
    this.db.collection('laporan').doc(this.userData.email).update(data).then(res=>{
      this.updatingProfileImage = false;
    }).catch(err=>{
      this.updatingProfileImage = false;
      this.toast.present('Tidak dapat mengganti foto profil, coba lagi.','top');
    })
  }

  updateProfileUrl(url)
  {    
    this.auth.onAuthStateChanged(res=>{
      res.updateProfile({photoURL:url});
    });
  }


  dismiss()
  {
    this.modalController.dismiss();
  }

  createReport(){
    this.db.collection('laporan').doc(this.userData.email).set(this.data)
  }

  async kirimLaporan(){
    this.router.navigate(['/dasboard'])
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Berhasil Mengirim Surat',
      message: 'Jika ada kendala dapat menghubungi Staff TU',
      buttons: [
        {
          text: 'Tutup',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        },
      ]
    });

    await alert.present();
  }

  
  async back(){
    this.router.navigate(['/dasboard'])
  }

  async report(){
    this.router.navigate(['/dasboard'])
  }

  chooseFile (event) {
    this.selectedFile = event.target.files
  }

 

  
}
