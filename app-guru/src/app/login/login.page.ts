import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth'
import firebase from 'firebase/app'
import { from } from 'rxjs';
import { Router } from '@angular/router'

import{ AlertController } from '@ionic/angular'

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  username: string = ""
  password: string = ""

  constructor(
    public auth: AngularFireAuth,
    public router: Router,
    public alert: AlertController
    
    ) { }

  ngOnInit() {
  }

  async login(){
    const { username, password } = this
    try {
        const res = await this.auth.signInWithEmailAndPassword(username + ("@firebase.com"), password)
        this.router.navigate(['/dasboard'])
    }catch(err) {
      console.dir(err)
      if(err.code === "auth/user-not-found") {
        this.showAlert("Error","Username/Password Salah!?")
          console.log("User not found")
      }
    }
  }

  async showAlert(header: string, message: string) {
    const alert = await this.alert.create({
      header,
      message,
      buttons: ["OK"]
    })

    await alert.present()
  }
  
  async forgetpass(){
    this.router.navigate(['/forget-pass'])
  }

}
