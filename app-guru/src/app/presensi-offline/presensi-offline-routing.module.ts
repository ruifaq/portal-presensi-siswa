import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresensiOfflinePage } from './presensi-offline.page';

const routes: Routes = [
  {
    path: '',
    component: PresensiOfflinePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PresensiOfflinePageRoutingModule {}
