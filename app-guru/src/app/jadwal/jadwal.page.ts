import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router'


@Component({
  selector: 'app-jadwal',
  templateUrl: './jadwal.page.html',
  styleUrls: ['./jadwal.page.scss'],
})
export class JadwalPage implements OnInit {

  data: any = {};
  userData: any = {};

  constructor(
    public router: Router,
    private db:AngularFirestore,
    public auth: AngularFireAuth,
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })
  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('user').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }


}
