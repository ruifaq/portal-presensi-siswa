import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForgetPassFinalPage } from './forget-pass-final.page';

const routes: Routes = [
  {
    path: '',
    component: ForgetPassFinalPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForgetPassFinalPageRoutingModule {}
