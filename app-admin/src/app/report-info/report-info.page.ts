import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFireStorage } from '@angular/fire/storage';
import { ModalController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-report-info',
  templateUrl: './report-info.page.html',
  styleUrls: ['./report-info.page.scss'],
})
export class ReportInfoPage implements OnInit {

  data: any = {};
  userData: any = {};
  laporanSiswa:any = {};
  laporanImage: any = [];
 id: any;
  imagePath: any;
  croppedImage: any;
  loading: HTMLIonLoadingElement;

  constructor(
    public router :Router,
    private db:AngularFirestore,
    private auth:AngularFireAuth,
    private route: ActivatedRoute,
    private storage: AngularFireStorage,
    private modalCtrl: ModalController,
    private loadingController: LoadingController,
  ) { }

  ngOnInit() {
    this.id=this.route.snapshot.paramMap.get('id')
    this.getLaporan(this.id);

    this.auth.onAuthStateChanged(user=> {
      this.userData = user;
    })

    this.getUrl()

    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })


  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('laporan').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }

  async info(){
    this.router.navigate(['/'])
  }

  async back(){
    this.router.navigate(['/report'])
  }

  async ok(){
    this.router.navigate(['/report'])
  }

  getLaporan(id){
    this.db.collection('laporan').doc(id).get().subscribe(res => {
      this.laporanSiswa = res.data();
      console.log(res.data())
    })
  }

  async unduhFile(id, file): Promise<any> {
    if(file && file.length) {
      try {
        await this.presentLoading();
        const task = await this.storage.ref('images').child(id).get(file[0])
        task.putString(this.croppedImage,'data_url').then(res=>{
          this.getUrl();
        })  
        this.loading.dismiss();
        this.router.navigate(['/dasboard'])
        return this.storage.ref(`images/${id}`).getDownloadURL().toPromise();
      } catch (error) {
        console.log(error);
      }
    }
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({
      message: 'Mohon Tunggu...'
    });
    return this.loading.present();
  }
  

  getUrl()
  {
    this.storage.ref(this.imagePath).getDownloadURL().subscribe(url=>{
  
      this.modalCtrl.dismiss({
        'imageUrl': url
      });
    },err=>{
        console.log('eror', err)
    })
  }

  

}
