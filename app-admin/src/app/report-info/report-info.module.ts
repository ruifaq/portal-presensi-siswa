import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReportInfoPageRoutingModule } from './report-info-routing.module';

import { ReportInfoPage } from './report-info.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReportInfoPageRoutingModule
  ],
  declarations: [ReportInfoPage]
})
export class ReportInfoPageModule {}
