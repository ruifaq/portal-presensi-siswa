import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore'
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-jadwal-edit',
  templateUrl: './jadwal-edit.page.html',
  styleUrls: ['./jadwal-edit.page.scss'],
})
export class JadwalEditPage implements OnInit {

  data: any = {};
  userData: any = {};
  jadwalSiswa:any = {};
  jadwal = [];
  id: any;
  
  constructor(
    public router: Router,
    private db:AngularFirestore,
    private auth:AngularFireAuth,
    private route: ActivatedRoute
 
  ) { }

  ngOnInit() {
    this.id=this.route.snapshot.paramMap.get('id')
    this.getJadwal(this.id);

    this.auth.onAuthStateChanged(user=>{
      this.userData = user;
    })

    this.db.collection('jadwal').snapshotChanges().subscribe(serverJadwal => {
      this.jadwal = [];
      serverJadwal.forEach(a => {
        let jdl:any = a.payload.doc.data();
        jdl.id = a.payload.doc.id;
        this.jadwal.push(jdl);
      })
    })
  }

  async back() {
    this.router.navigate(['/jadwal'])
  }

  getJadwal(id){
    this.db.collection('jadwal').doc(id).get().subscribe(res => {
      this.jadwalSiswa = res.data();
      console.log(res.data())
    })
  }
   
  editJadwal(id: any, data:String){
    this.db.collection('jadwal').doc(this.id).update(this.jadwalSiswa).then(() => {
      alert('Berhasil mengedit data...!')
    }).catch(function(error) {
      console.error("Error, Gagal Edit Data...!?", error);
    });
    this.router.navigate(['/jadwal'])
  }
  
  

}
