import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { JadwalEditPage } from './jadwal-edit.page';

describe('JadwalEditPage', () => {
  let component: JadwalEditPage;
  let fixture: ComponentFixture<JadwalEditPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JadwalEditPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(JadwalEditPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
