import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController, ModalController } from '@ionic/angular';
import { ToastService } from '../services/toast.service';
import { AlertService } from '../services/alert.service';


@Component({
  selector: 'app-report',
  templateUrl: './report.page.html',
  styleUrls: ['./report.page.scss'],
})
export class ReportPage implements OnInit {

  data: any = {};
  userData: any = {};
  laporanSiswa: any = [];
  public mapelList: any[];
  public loadedMapelList: any[];

  constructor(
    public router: Router,
    private db: AngularFirestore,
    private auth: AngularFireAuth,
    public alert: AlertService,
    public alertController: AlertController,
    public modalController: ModalController,
    public toast: ToastService
  ) { }

  ngOnInit() {

    this.getLaporan();

    this.db.collection("laporan").valueChanges({ idField: 'id' }).subscribe(mapelList => {
      this.mapelList = mapelList;
      this.loadedMapelList = mapelList;
    })

  }

  initializeItems(): void {
    this.mapelList = this.loadedMapelList;
  }

  filterList(evt) {
    this.initializeItems();

    const searchTerm = evt.srcElement.value;

    if (!searchTerm) {
      return;
    }

    this.mapelList = this.mapelList.filter(currentMapel => {
      if (currentMapel.name && searchTerm) {
        if (currentMapel.name.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  async back() {
    this.router.navigate(['/dasboard'])
  }

  async info(id) {
    this.router.navigate(['/report-info'])
  }

  getLaporan() {
    this.laporanSiswa = [];
    this.db.collection('laporan').valueChanges({ idField: 'id' }).subscribe(res => {
      this.laporanSiswa = res;
    })
  }

}
