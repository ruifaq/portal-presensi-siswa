import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TambahDataMapelPageRoutingModule } from './tambah-data-mapel-routing.module';

import { TambahDataMapelPage } from './tambah-data-mapel.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TambahDataMapelPageRoutingModule
  ],
  declarations: [TambahDataMapelPage]
})
export class TambahDataMapelPageModule {}
