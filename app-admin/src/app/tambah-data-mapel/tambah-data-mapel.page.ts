import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore'
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-tambah-data-mapel',
  templateUrl: './tambah-data-mapel.page.html',
  styleUrls: ['./tambah-data-mapel.page.scss'],
})
export class TambahDataMapelPage implements OnInit {

  data: any = {};
  userData: any = {};
  jadwal = [];

  constructor(
    public router: Router,
    private db:AngularFirestore,
    private auth:AngularFireAuth
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(user=>{
      this.userData = user;
    })

    this.db.collection('jadwal').snapshotChanges().subscribe(serverJadwal => {
      this.jadwal = [];
      serverJadwal.forEach(a => {
        let jdl:any = a.payload.doc.data();
        jdl.id = a.payload.doc.id;
        this.jadwal.push(jdl);
      })
    })

  }

  async back(){
    this.router.navigate(['/jadwal'])
  }

  async tambahData(){
    this.router.navigate(['/jadwal'])
  }

  tambahJadwal(){
    this.data.author = this.userData.email;
    var doc = new Date().getTime().toString();
    this.db.collection('jadwal').doc(doc).set(this.data).then(res=>{
      this.router.navigate(['/jadwal'])
    })
  }

}
