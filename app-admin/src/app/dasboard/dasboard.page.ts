import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router'

@Component({
  selector: 'app-dasboard',
  templateUrl: './dasboard.page.html',
  styleUrls: ['./dasboard.page.scss'],
})
export class DasboardPage implements OnInit {
  count: number = 0;
  buttonDisabled: boolean = false;
  
  data: any = {};
  userData: any = {};
  jadwalSiswa:any = [];
  uid:any = [];

  constructor(
    public router: Router,
    private db:AngularFirestore,
  ) { }

  ngOnInit() {
  }

  async jadwal(){
    this.router.navigate(['/jadwal'])
  }

  async rekap(){
    this.router.navigate(['/rekap'])
  }

  async report(){
    this.router.navigate(['/report'])
  }

  async presensi(){
    this.router.navigate(['/presensi-form'])
  }

  async profile(){
    this.router.navigate(['/profile'])
  }

  disable(){
    this.router.navigate([''])
this.buttonDisabled = true
  }

  bukaPresensi(aktif){
   
    this.db.collection('jadwal').doc(this.data).update(this.data.aktif).then(res=> {
      status = "true" + res; 
    });
  }

}
