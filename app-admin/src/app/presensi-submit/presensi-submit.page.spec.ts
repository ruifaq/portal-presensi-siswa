import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PresensiSubmitPage } from './presensi-submit.page';

describe('PresensiSubmitPage', () => {
  let component: PresensiSubmitPage;
  let fixture: ComponentFixture<PresensiSubmitPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresensiSubmitPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PresensiSubmitPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
