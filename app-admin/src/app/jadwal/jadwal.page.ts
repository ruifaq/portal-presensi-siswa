import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore'
import { AngularFireAuth } from '@angular/fire/auth';
import { removeListener } from 'process';
import { NavController } from '@ionic/angular';




@Component({
  selector: 'app-jadwal',
  templateUrl: './jadwal.page.html',
  styleUrls: ['./jadwal.page.scss'],
})
export class JadwalPage implements OnInit {

  data: any = {};
  userData: any = {};
  jadwalSiswa:any = [];
  uid:any = [];
  public mapelList: any[];
  public loadedMapelList: any[];

  constructor(
    public router: Router,
    private db:AngularFirestore,
    private auth:AngularFireAuth,
    private navCtrl: NavController
 
 
  ) { }

  ngOnInit() {

    this.getJadwal();

    this.db.collection("jadwal").valueChanges({idField: 'id'}).subscribe(mapelList => {
      this.mapelList = mapelList;
      this.loadedMapelList = mapelList;
    })
    
    
  }

  
  initializeItems(): void{
    this.mapelList = this.loadedMapelList;
  }

  filterList(evt){
    this.initializeItems();

    const searchTerm = evt.srcElement.value;

    if(!searchTerm) {
      return;
    }

    this.mapelList = this.mapelList.filter(currentMapel => {
      if(currentMapel.mapel && searchTerm) {
        if(currentMapel.mapel.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  async tambahData(){
    this.router.navigate(['/tambah-data-mapel'])
  }

  editJadwal(id){
    this.router.navigate(['/jadwal-edit'])
    
  }

  getJadwal(){
    this.jadwalSiswa = [];
    this.db.collection('jadwal').valueChanges({idField: 'id'}).subscribe(res => {
      this.jadwalSiswa = res;
 
    })
  }

  hapusJadwal(id){
  this.db.collection('jadwal').doc(id).delete().then(function() {
    console.log("berhasil hapus...");
    alert('Berhasil menghapus data...!')
  }).catch(function(error) {
    console.error("Eror bund", error);
  });
  }

  update(id, mapel, kelas, jam, guru){
  console.log(id, mapel, kelas, jam, guru);
    this.navCtrl.navigateForward(['/jadwal-edit'])
  }
  
  bukaPresensi(jam){
    const A = 5;
    if( A < 50){
      this.router.navigate(['/presensi-form'])
    }else{
      this.router.navigate(['/presensi-late'])
    }
  }
  

}
