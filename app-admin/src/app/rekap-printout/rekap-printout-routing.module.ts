import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RekapPrintoutPage } from './rekap-printout.page';

const routes: Routes = [
  {
    path: '',
    component: RekapPrintoutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RekapPrintoutPageRoutingModule {}
