import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { jsPDF } from "jspdf";
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;
import htmlToPdfmake from 'html-to-pdfmake';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-rekap-printout',
  templateUrl: './rekap-printout.page.html',
  styleUrls: ['./rekap-printout.page.scss'],
})
export class RekapPrintoutPage implements OnInit {


  jadwalSiswa:any = [];
 presensiSiswa:any = {};
  id: any;
  date = new Date();

  @ViewChild('pdfTable') pdfTable: ElementRef;

  constructor(
    public router: Router,
    private db:AngularFirestore,
    public route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getJadwal()

    this.id=this.route.snapshot.paramMap.get('id')
    this.getRekap(this.id);
  }

  async back() {
    this.router.navigate(['/rekap'])
  }

  /*download() {
    const doc = new jsPDF();
    document.getElementById('invoice')
    const invoice = document.getElementById("invoice");
    console.log(invoice);
    console.log(window);

    doc.save("a4.pdf");
  }*/

  public downloadAsPDF() {
    const doc = new jsPDF();
    //get table html
    const pdfTable = this.pdfTable.nativeElement;
    //html to pdf format
    var html = htmlToPdfmake(pdfTable.innerHTML);
   
    const documentDefinition = { content: html };
    pdfMake.createPdf(documentDefinition).open();
  }

  getJadwal(){
    this.jadwalSiswa = [];
    this.db.collection('jadwal').valueChanges({idField: 'id'}).subscribe(res => {
      this.jadwalSiswa = res;
      
    })
  }

  getRekap(id){
    this.db.collection('jadwal').doc(id).get().subscribe(res => {
      this.presensiSiswa = res.data();
      console.log(res.data())
    })
  }
}
