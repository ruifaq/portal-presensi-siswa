import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RekapPrintoutPage } from './rekap-printout.page';

describe('RekapPrintoutPage', () => {
  let component: RekapPrintoutPage;
  let fixture: ComponentFixture<RekapPrintoutPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RekapPrintoutPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RekapPrintoutPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
