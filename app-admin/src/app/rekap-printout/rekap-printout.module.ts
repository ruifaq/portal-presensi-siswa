import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RekapPrintoutPageRoutingModule } from './rekap-printout-routing.module';

import { RekapPrintoutPage } from './rekap-printout.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RekapPrintoutPageRoutingModule
  ],
  declarations: [RekapPrintoutPage]
})
export class RekapPrintoutPageModule {}
