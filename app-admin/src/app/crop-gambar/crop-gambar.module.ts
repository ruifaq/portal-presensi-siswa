import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CropGambarPageRoutingModule } from './crop-gambar-routing.module';

import { CropGambarPage } from './crop-gambar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CropGambarPageRoutingModule
  ],
  declarations: [CropGambarPage]
})
export class CropGambarPageModule {}
