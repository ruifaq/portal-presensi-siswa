import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CropGambarPage } from './crop-gambar.page';

describe('CropGambarPage', () => {
  let component: CropGambarPage;
  let fixture: ComponentFixture<CropGambarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CropGambarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CropGambarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
