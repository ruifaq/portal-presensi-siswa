import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresensiFormPage } from './presensi-form.page';

const routes: Routes = [
  {
    path: '',
    component: PresensiFormPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PresensiFormPageRoutingModule {}
