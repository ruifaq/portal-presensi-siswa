import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PotongGambarPage } from './potong-gambar.page';

const routes: Routes = [
  {
    path: '',
    component: PotongGambarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PotongGambarPageRoutingModule {}
