import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PotongGambarPageRoutingModule } from './potong-gambar-routing.module';

import { PotongGambarPage } from './potong-gambar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PotongGambarPageRoutingModule
  ],
  declarations: [PotongGambarPage]
})
export class PotongGambarPageModule {}
