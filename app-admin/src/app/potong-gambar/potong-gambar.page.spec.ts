import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PotongGambarPage } from './potong-gambar.page';

describe('PotongGambarPage', () => {
  let component: PotongGambarPage;
  let fixture: ComponentFixture<PotongGambarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PotongGambarPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PotongGambarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
