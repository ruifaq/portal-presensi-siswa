import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PresensiOfflinePage } from './presensi-offline.page';

describe('PresensiOfflinePage', () => {
  let component: PresensiOfflinePage;
  let fixture: ComponentFixture<PresensiOfflinePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresensiOfflinePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PresensiOfflinePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
