import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PresensiOfflinePageRoutingModule } from './presensi-offline-routing.module';

import { PresensiOfflinePage } from './presensi-offline.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PresensiOfflinePageRoutingModule
  ],
  declarations: [PresensiOfflinePage]
})
export class PresensiOfflinePageModule {}
