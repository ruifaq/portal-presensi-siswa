import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Router } from '@angular/router'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-rekap',
  templateUrl: './rekap.page.html',
  styleUrls: ['./rekap.page.scss'],
})
export class RekapPage implements OnInit {


  data: any = {};
  userData: any = {};
  items: Observable<any[]>;
  newTodo: string = '';
  itemsRef: AngularFirestoreCollection;
  selectedFile: any;
  jadwalSiswa:any = [];
  public mapelList: any[];
  public loadedMapelList: any[];

  constructor(
    public router: Router,
    private auth:AngularFireAuth,
    private db:AngularFirestore,
  ) { }

  ngOnInit() {
    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })

    this.db.collection("jadwal").valueChanges().subscribe(mapelList => {
      this.mapelList = mapelList;
      this.loadedMapelList = mapelList;
    })

    this.getJadwal();
  }

  initializeItems(): void{
    this.mapelList = this.loadedMapelList;
  }

  filterList(evt){
    this.initializeItems();

    const searchTerm = evt.srcElement.value;

    if(!searchTerm) {
      return;
    }

    this.mapelList = this.mapelList.filter(currentMapel => {
      if(currentMapel.mapel && searchTerm) {
        if(currentMapel.mapel.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1) {
          return true;
        }
        return false;
      }
    });
  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('user').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  async print(){
    this.router.navigate(['/rekap-print'])
  }

  getJadwal(){
    this.jadwalSiswa = [];
    this.db.collection('jadwal').valueChanges({idField: 'id'}).subscribe(res => {
      this.jadwalSiswa = res;
      
    })
  }
  
}
