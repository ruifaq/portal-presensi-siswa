import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PresensiLatePageRoutingModule } from './presensi-late-routing.module';

import { PresensiLatePage } from './presensi-late.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PresensiLatePageRoutingModule
  ],
  declarations: [PresensiLatePage]
})
export class PresensiLatePageModule {}
