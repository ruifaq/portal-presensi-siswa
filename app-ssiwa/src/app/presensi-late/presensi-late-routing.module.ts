import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresensiLatePage } from './presensi-late.page';

const routes: Routes = [
  {
    path: '',
    component: PresensiLatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PresensiLatePageRoutingModule {}
