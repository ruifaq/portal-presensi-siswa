import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CropGambarPage } from './crop-gambar.page';

const routes: Routes = [
  {
    path: '',
    component: CropGambarPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CropGambarPageRoutingModule {}
