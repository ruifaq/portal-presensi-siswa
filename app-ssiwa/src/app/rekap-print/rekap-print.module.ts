import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RekapPrintPageRoutingModule } from './rekap-print-routing.module';

import { RekapPrintPage } from './rekap-print.page';
import { File } from '@ionic-native/file'


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RekapPrintPageRoutingModule,
    ReactiveFormsModule,

  ],
  declarations: [RekapPrintPage]
})
export class RekapPrintPageModule {}
