import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RekapPrintPage } from './rekap-print.page';

describe('RekapPrintPage', () => {
  let component: RekapPrintPage;
  let fixture: ComponentFixture<RekapPrintPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RekapPrintPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RekapPrintPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
