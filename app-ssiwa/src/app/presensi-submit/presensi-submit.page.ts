import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-presensi-submit',
  templateUrl: './presensi-submit.page.html',
  styleUrls: ['./presensi-submit.page.scss'],
})
export class PresensiSubmitPage implements OnInit {

  data: any = {};
  userData: any = {};
  jadwalSiswa: any = [];

  constructor(
    public router: Router,
    private db:AngularFirestore,
    private auth:AngularFireAuth,
    public alertController: AlertController,
  ) { }

  ngOnInit() {
    this.getPresensi();
  }

  async back(){
    this.router.navigate(['/presensi-form'])
  }

  async next(){
    this.router.navigate(['/dasboard'])
  }

  getPresensi(){
    this.jadwalSiswa = [];
    this.db.collection('events').valueChanges({idField: 'id'}).subscribe(res => {
      this.jadwalSiswa = res;
    })
  }

  async kirimReport(){
    this.router.navigate(['/dasboard'])
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Berhasil Mengisi Presensi',
      message: 'Jika ada kendala dapat menghubungi Guru Mapel atau Wali Kelas',
      buttons: [
        {
          text: 'Tutup',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            return false;
          }
        },
      ]
    });

    await alert.present();
  }

}
