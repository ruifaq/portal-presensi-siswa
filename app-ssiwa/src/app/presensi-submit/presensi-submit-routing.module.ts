import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PresensiSubmitPage } from './presensi-submit.page';

const routes: Routes = [
  {
    path: '',
    component: PresensiSubmitPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PresensiSubmitPageRoutingModule {}
