import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PresensiSubmitPageRoutingModule } from './presensi-submit-routing.module';

import { PresensiSubmitPage } from './presensi-submit.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PresensiSubmitPageRoutingModule
  ],
  declarations: [PresensiSubmitPage]
})
export class PresensiSubmitPageModule {}
