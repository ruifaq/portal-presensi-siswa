import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'


@Component({
  selector: 'app-presensi-qrcode',
  templateUrl: './presensi-qrcode.page.html',
  styleUrls: ['./presensi-qrcode.page.scss'],
})
export class PresensiQrcodePage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async kirim(){
    this.router.navigate(['/presensi-submit'])
  }

}
