import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PresensiQrcodePage } from './presensi-qrcode.page';

describe('PresensiQrcodePage', () => {
  let component: PresensiQrcodePage;
  let fixture: ComponentFixture<PresensiQrcodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PresensiQrcodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PresensiQrcodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
