import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ForgetPassPhonePage } from './forget-pass-phone.page';

const routes: Routes = [
  {
    path: '',
    component: ForgetPassPhonePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ForgetPassPhonePageRoutingModule {}
