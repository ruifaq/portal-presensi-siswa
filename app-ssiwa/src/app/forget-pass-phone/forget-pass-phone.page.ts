import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-forget-pass-phone',
  templateUrl: './forget-pass-phone.page.html',
  styleUrls: ['./forget-pass-phone.page.scss'],
})
export class ForgetPassPhonePage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

  async forgetfinal(){
    this.router.navigate(['/forget-pass-final'])
  }

}
