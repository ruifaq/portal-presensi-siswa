import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ForgetPassPhonePage } from './forget-pass-phone.page';

describe('ForgetPassPhonePage', () => {
  let component: ForgetPassPhonePage;
  let fixture: ComponentFixture<ForgetPassPhonePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgetPassPhonePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ForgetPassPhonePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
