import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth'
import firebase from 'firebase/app'
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFirestore } from '@angular/fire/firestore';


@Component({
  selector: 'app-regis',
  templateUrl: './regis.page.html',
  styleUrls: ['./regis.page.scss'],
})
export class RegisPage implements OnInit {

  username: string = ""
  password: string = ""
  cpassword: string = ""
  data: any = {};
  userData: any = {};

  constructor(
    public auth: AngularFireAuth,   
    private afDB: AngularFireDatabaseModule,
    private db:AngularFirestore,
    ) { }

  ngOnInit() {
  }

  async regis() {
    const { username, password, cpassword } = this
    if(password !== cpassword) {
      return console.log("Password salah!?")
      
    }
    try {
      const res = await this.auth.createUserWithEmailAndPassword(username + ("@firebase.com"), password).then((cek)=> {
        this.db.collection('user').doc(cek.user.email).set(this.data).then(res =>{
          this.username
          this.password
        })
      })
      console.log(res) 
    }catch(error) {
      console.dir(error)
    }
  
  }

}
