import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'
import { HttpClient } from '@angular/common/http'
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-jadwal',
  templateUrl: './jadwal.page.html',
  styleUrls: ['./jadwal.page.scss'],
})
export class JadwalPage implements OnInit {

  data: any = {};
  userData: any = {};
  jadwalSiswa: any = [];

  constructor(
    public router: Router,
    public http: HttpClient,
    private db:AngularFirestore,
    private auth:AngularFireAuth,
  ) { }

  ngOnInit() {

    this.getJadwal();

    this.auth.onAuthStateChanged(res=>{
      this.userData = res;
      this.getUser(res.email);
    })
  }

  user:any = {};
  getUser(email)
  {
    this.db.collection('user').doc(email).valueChanges().subscribe(res=>{
      this.user = res;
    })
  }

  async back(){
    this.router.navigate(['/dasboard'])
  }

  getJadwal(){
    this.jadwalSiswa = [];
    this.db.collection('jadwal').valueChanges({idField: 'id'}).subscribe(res => {
      this.jadwalSiswa = res;
    })
  }

}
