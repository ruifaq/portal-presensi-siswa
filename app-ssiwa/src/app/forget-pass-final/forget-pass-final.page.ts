import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

@Component({
  selector: 'app-forget-pass-final',
  templateUrl: './forget-pass-final.page.html',
  styleUrls: ['./forget-pass-final.page.scss'],
})
export class ForgetPassFinalPage implements OnInit {

  constructor(
    public router: Router
  ) { }

  ngOnInit() {
  }

async backlogin(){
  this.router.navigate(['/login'])
}

}
